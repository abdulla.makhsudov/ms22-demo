package com.example.bankapp.mapper;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.entity.Account;
import com.example.bankapp.entity.Card;
import java.util.Set;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AccountMapper {

    @Mapping(target = "username", source = "user.username")
    @Mapping(target = "password", source = "user.password")
    @Mapping(target = "cardNumber", source = "cards", qualifiedByName = "setCardNumber")
    @Mapping(target = "cardType", source = "cards", qualifiedByName = "setCarType")
    @Mapping(target = "expirationDate", source = "cards", qualifiedByName = "setCardDate")
    AccountDto entityToDto(Account account);

    @Named("setCardNumber")
    default String setCardNumber(Set<Card> cards) {
        return cards.stream().findFirst().get().getCardNumber();
    }

    @Named("setCarType")
    default String setCarType(Set<Card> cards) {
        return cards.stream().findFirst().get().getCardType();
    }

    @Named("setCardDate")
    default String setCardDate(Set<Card> cards) {
        return cards.stream().findFirst().get().getExpirationDate();
    }

}
