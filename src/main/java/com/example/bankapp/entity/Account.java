package com.example.bankapp.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.NamedSubgraph;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import java.util.Set;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "accounts")
@Data
@NamedEntityGraph(name = "account-with-user-cards-benefits", attributeNodes = {
        @NamedAttributeNode(value = "user"),
        @NamedAttributeNode(value = "cards", subgraph = "cards.benefits")
}, subgraphs = {
        @NamedSubgraph(name = "cards.benefits", attributeNodes = {
                @NamedAttributeNode(value = "benefits")
        })

})
//@NamedQuery(name = "test", query = "select a from Account a")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    private String accountNumber;
    private double balance;

    @Version
    private Long version;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private Set<Card> cards;
}