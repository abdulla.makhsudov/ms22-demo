package com.example.bankapp.repository;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.entity.Account;
import jakarta.persistence.LockModeType;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

public interface AccountRepository extends JpaRepository<Account, Long> {
//    Optional<Account> findByAccountNumber(String accoountNumber);

    @Query("select a from Account a join fetch a.user u join fetch a.cards c join fetch c.benefits b")
    List<Account> findByCustom();

    @Query(value = "select * from accounts a\n" +
            "    join users u on u.id=a.user_id\n" +
            "    join cards c on a.id=c.account_id\n" +
            "    join card_benefits b on c.id=b.card_id;", nativeQuery = true)
    List<Account> findByCustomNative();

    @Query("select new com.example.bankapp.dto.AccountDto(a.id, a.accountNumber, a.balance, " +
            "u.username, u.password, " +
            "c.cardNumber, c.cardType, c.expirationDate) from Account a join  a.user u join  a.cards c join  c.benefits b")
    List<AccountDto> findAllCustom();

    @EntityGraph(attributePaths = {"user", "cards", "cards.benefits"})
    List<Account> findByAccountNumber(String accountNumber);

    @EntityGraph(value = "account-with-user-cards-benefits")
    @Query("select a from Account a")
    List<Account> findByCustomGraph();

    @Lock(LockModeType.OPTIMISTIC_FORCE_INCREMENT)
    @Override
    Optional<Account> findById(Long aLong);

    @Query(value = "select * from accounts a where a.balance=:balance", nativeQuery = true)
    Account findByBalance(Double balance);
}
