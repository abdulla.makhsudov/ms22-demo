package com.example.bankapp;

import com.example.bankapp.entity.Authority;
import com.example.bankapp.entity.User;
import com.example.bankapp.repository.AccountRepository;
import com.example.bankapp.repository.StudentRepository;
import com.example.bankapp.repository.UserRepository;
import com.example.bankapp.service.StudentService;
import com.example.bankapp.service.impl.AccountServiceImpl;
import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
@EnableCaching
@OpenAPIDefinition(info = @Info(
        title = "Bank app",
        description = "Bank app for customers",
        contact = @Contact(
                name = "Resul",
                url = "https//google.com"
        )
), externalDocs = @ExternalDocumentation(
        url = "https://test.com",
        description = "desc test"
)
)
public class BankAppApplication implements CommandLineRunner {

    private final AccountRepository accountRepository;
    private final StudentService studentService;
    private final UserRepository userRepository;
    private final StudentRepository studentRepository;
    @Autowired
    private AccountServiceImpl accountService;

    private final BCryptPasswordEncoder passwordEncoder;


    public static void main(String[] args) {
        SpringApplication.run(BankAppApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        String accountNumber = "iba321";
        Double balance = 900.00;
//        saveAccount(accountNumber, balance);


//        Student student1 = studentRepository.findById(1L).get();
//        Student student2 = studentRepository.findById(1L).get();

        Authority admin = new Authority();
        admin.setAuthority("ADMIN");

        User qulu = new User();
        qulu.setUsername("qulu");
        qulu.setPassword(passwordEncoder.encode("123"));
        qulu.setCredentialsNonExpired(true);
        qulu.setEnabled(true);
        qulu.setAccountNonExpired(true);
        qulu.setAccountNonLocked(true);
        qulu.setAuthorities(Set.of(admin));

        userRepository.save(qulu);

    }

    //    @Transactional
//    public void saveAccount(String accountNumber, Double balance) {
//        WeakReference<String> test = new WeakReference<>("SALAM");
//        SoftReference<String> test2 = new SoftReference<>("");
//        ReferenceQueue<String> queue = new ReferenceQueue<>();
//        PhantomReference<String> test22 = new PhantomReference<>("S", queue);
//        String s = "Salam" + "test";
//
//        Account account = accountRepository.findById(3L).get();
//        account.setAccountNumber(accountNumber);
//        account.setBalance(balance);
//        accountRepository.save(account);
//    }
}
