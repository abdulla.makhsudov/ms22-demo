package com.example.bankapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {
    private Long id;
    @Schema(type = "string", example = "iba123")
    private String accountNumber;
    private double balance;
    private String username;
    private String password;
    private String cardNumber;
    private String cardType;
    private String expirationDate;
}
