package com.example.bankapp.service.impl;

import com.example.bankapp.entity.User;
import com.example.bankapp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userDb = userRepository.findByUsername(username).orElseThrow();
//        UserDetails userDetails = org.springframework.security.core.userdetails.User.builder()
//                .username(userDb.getUsername())
//                .password(userDb.getPassword())
//                .authorities(List.of())
//                .build();
        return userDb;
    }
}
