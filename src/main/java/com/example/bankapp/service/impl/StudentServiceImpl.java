package com.example.bankapp.service.impl;

import com.example.bankapp.dto.SearchCriteria;
import com.example.bankapp.entity.Student;
import com.example.bankapp.repository.StudentRepository;
import com.example.bankapp.service.StudentService;
import com.example.bankapp.spec.StudentSpecification;
import jakarta.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public Long saveStudent(Student student) {
        return studentRepository.save(student).getId();
    }

    @Override
    public List<Student> getAllStudents(Student student) {

        Specification<Student> studentSpecification = null;


        studentSpecification = (root, cq, cb) -> {

            List<Predicate> predicateList = new ArrayList<>();

            if (student.getName() != null) {
                predicateList.add(cb.equal(root.get("name"), student.getName()));
            }

            if (student.getSurname() != null) {
                predicateList.add(cb.equal(root.get("surname"), student.getSurname()));
            }

            cq.where(cb.and(predicateList.toArray(new Predicate[0])));
            return cq.getRestriction();

        };

        return studentRepository.findAll(studentSpecification);
    }

    @Override
    public List<Student> getStudents(String name, String surname, Long age, String gender) {
        return studentRepository.getStudnets(name, surname, age, gender);
    }

    @Override
    public List<Student> findAllStudents(List<SearchCriteria> searchCriteriaList) {
        StudentSpecification studentSpecification = new StudentSpecification();
        searchCriteriaList.forEach(searchCriteria -> studentSpecification.add(searchCriteria));
        return studentRepository.findAll(studentSpecification);
    }

    @Override
    public Page<Student> getStudentAll(int pageSize, int pageNumber, String[] pageSort) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(pageSort[0]).descending());
        return studentRepository.findAll(pageable);
    }

    @CachePut(cacheNames = "student", key = "#student.name.length()")
    @Override
    @Transactional
    public Student updateStudent(Student student, Long id) {
        if (studentRepository.findById(id).isPresent()) {
            Student student1 = studentRepository.findById(id).get();
            student1.setName(student.getName());
            student1.setAge(student.getAge());
            return student1;
        }
        return null;
    }
}
