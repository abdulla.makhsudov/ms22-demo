package com.example.bankapp.service;

import com.example.bankapp.entity.User;

public interface UserService {

    User getUser(Long id);
}
