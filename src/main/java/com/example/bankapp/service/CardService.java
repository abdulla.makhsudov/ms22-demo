package com.example.bankapp.service;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.repository.AccountRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardService {

    private final AccountRepository accountRepository;

    public List<AccountDto> getAccount() {
        return accountRepository.findAllCustom();
    }
}
